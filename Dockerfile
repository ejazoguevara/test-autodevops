FROM node:14.15.0-alpine

WORKDIR /app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /app
RUN npm install

COPY . .

ENV PORT 5000
EXPOSE $PORT
CMD [ "npm", "start" ]